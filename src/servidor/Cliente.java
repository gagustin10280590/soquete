/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package servidor;

/**
 *
 * @author gilberto
 */
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.net.*;
import java.io.*;
import javax.swing.border.Border;

public class Cliente extends JFrame implements ActionListener{

    Container c;
    JTextField sumando1,sumando2;
    JLabel titulo1,introduceSumando1,introduceSumando2;
    JPanel titulo,datos,resultado;
    JPanel p1,p2,p3,p4;
    JButton boton;
    JTextArea display;
    
    public Cliente()
    {
        setTitle("Cliente de socket");
        setLayout(new GridLayout(3,1));
        c= getContentPane();
        c.setBackground(Color.BLUE);
        titulo= new JPanel();
        datos=new JPanel();
        resultado=new JPanel();
        sumando1=new JTextField(" ",20);
        sumando2=new JTextField(" ", 20);
        titulo1=new JLabel("Introduce los isguientes datos");
        introduceSumando1= new JLabel("Primero sumando:");
        introduceSumando2= new JLabel("Segundo sumando:");
        boton=new JButton("Servidor has la suma");
        display=new JTextArea(30,30);
        //acomodamos paneles en el componente        
        c.add(titulo);
        c.add(datos);
        c.add(resultado);
        
        //agregamos etiqutes y demas a los paneles
        
        titulo.add(titulo1);
        datos.add(introduceSumando1);
        datos.add(sumando1);
        datos.add(introduceSumando2);
        datos.add(sumando2);
        datos.add(boton);
        resultado.add(display);
                
        
                
        // de las componentes GUI

          pack();
        this.setLocationRelativeTo(null);
        setVisible(true);
        setSize(500,600);
        setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        boton.addActionListener(this);
        
    }         

 @Override
    public void actionPerformed(ActionEvent e) {
       double s1,s2;    
    	s1=Double.parseDouble(sumando1.getText());
    	s2=Double.parseDouble(sumando2.getText());
    	System.out.println("Haciendo petición");
    	peticionServidor(s1,s2);
    }
    
    private void peticionServidor(double s1, double s2) {
        // declaración de un objecto para el socket cliente
        Socket client;
        Socket ip=null;
        DataInputStream input;
        DataOutputStream output;
        InetAddress direccion;

        // declaración de los objetos para el flujo de datos
        double suma;
        String Suma;
        try {;
            
           client = new Socket(InetAddress.getLocalHost(), 6000);
            //client = new Socket("127.0.0.1", 6000) ;
             //CONTRUCTOR QUE REGRESA IP Y EL PUERTO logico"socket"
            // creación de la instancia del socket
            display.setText("Socket Creado....\n");
            // creación de las instancias para el flujo de datos
            input = new DataInputStream(client.getInputStream());
            output = new DataOutputStream(client.getOutputStream());

            display.append("Enviando primer sumando\n");
            output.writeDouble(s1);
            display.append("Enviando segundo sumando\n");
            output.writeDouble(s2);
            display.append("El servidor dice....\n\n");
            suma = input.readDouble();
            Suma = String.valueOf(suma);
            display.append("El  resultado es: " + Suma + "\n\n");
            display.append("Cerrando cliente\n\n");
            client.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public static void main(String args[])
    {
        new Cliente();
        
    }
}

